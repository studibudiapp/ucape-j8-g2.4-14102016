package c5
     
import org.jcsp.lang.*
import org.jcsp.groovy.*
import c05.ScaledData     
   
class Scale implements CSProcess {
  def int scaling
  def ChannelOutput outChannel
  def ChannelOutput factor
  def ChannelInput inChannel
  def ChannelInput suspend
  def ChannelInput injector
  
  void run () {
    def SECOND = 1000
    def DOUBLE_INTERVAL = 5 * SECOND
    def SUSPEND  = 0
    def INJECT   = 1
    def TIMER    = 2
    def INPUT    = 3
    
    def timer = new CSTimer()
    def scaleAlt = new ALT ( [ suspend, injector, timer, inChannel ] )
    
    def preCon = new boolean [4]
    preCon[SUSPEND] = true
    preCon[INJECT] = false
    preCon[TIMER] = true
    preCon[INPUT] = true
    def suspended = false
                                                                    
    def timeout = timer.read() + DOUBLE_INTERVAL
    timer.setAlarm ( timeout )
    
    while (true) {
      switch ( scaleAlt.priSelect(preCon) ) {
        case SUSPEND :
          	// Deal with suspend input 
			suspend.read()
			// Scale sends scaling factor to controller
			factor.write(scaling)
			// set precondition for inject = true
			preCon[INJECT] = true
			// set precondition for timer = false
			preCon[TIMER] = false
			// set precondition for suspend = false
			preCon[SUSPEND] = false
			// set suspended to true so that Scale sends numbers through unscaled
			suspended = true
			// print that scaling is suspended
          	println "Suspend: Scaling is suspended"
          break
        case INJECT:
        	// Deal with inject input
			// Read inject value and set scaling factor to new value
			scaling = injector.read()
			// set suspended to false
			suspended = false
			// set precondition for inject = false
			preCon[INJECT] = false
			// set precondition for timer = true
			preCon[TIMER] = true
			// set precondition for suspend = true
			preCon[SUSPEND] = true
			// Reset timer
			timeout = timer.read() + DOUBLE_INTERVAL
			timer.setAlarm ( timeout )
			// Return to normal function
			// print the new scaling factor
          	println "Injection complete: new scaling is ${scaling}"
          break
        case TIMER:
        	// Deal with Timer input
			timeout = timer.read() + DOUBLE_INTERVAL
			timer.setAlarm ( timeout )
			// Double scaling factor
			scaling *= 2
			// print new scaling factor
          	println "Normal Timer: new scaling is ${scaling}"
		  	// continue with normal input
          break
        case INPUT:
          	// Deal with Input channel 
			def inValue = inChannel.read()
			def result = new ScaledData()
			result.original = inValue
			// if suspended is true, output unfactored input, else output input * scaling
			if(suspended)
			{
				result.scaled = inValue
			}
			else
			{
				result.scaled = inValue * scaling
			}
			outChannel.write ( result )
          break
      } //end-switch
    } //end-while
  } //end-run
}