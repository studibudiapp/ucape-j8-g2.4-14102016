package c2

import org.jcsp.lang.*

class CreateSetsOfEight implements CSProcess{
	
	def ChannelInput inChannel
	def results = []
	
	void run(){
		def outList = []
		def v = inChannel.read()
		def iteration = 0
		while (v != -1){
			for ( i in 0 .. 7 ) {
				// put v into outList and read next input
				outList.add(v)
				v = inChannel.read()
			}
			println " Eight Object is ${outList}"
			results[iteration] = outList
			iteration++
			outList = []
		}
		println "Finished"
	}
}