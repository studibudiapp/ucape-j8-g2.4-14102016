package c25

import org.jcsp.groovy.*
import org.jcsp.lang.*
import java.awt.event.*
import java.awt.event.MouseEvent

class MouseBuffer implements CSProcess {
	ChannelInput mouseEvent
	ChannelInput getPoint
	ChannelOutput sendPoint
	
	void run(){
		def alt = new ALT([getPoint, mouseEvent])
		def preCon = new boolean[2]
		def GET = 0
		preCon[GET] = false
		preCon[1] = true
		def point
		def disable = true
		
		while (true)
			{
			switch ( alt.select(preCon)) 
			{
				case GET :
					switch(getPoint.read())
					{
						case "disable":
							disable = true
							break
						case "enable":
							disable = false
							break
						default:
							sendPoint.write(new MousePoint (point: point))
							preCon[GET] = false
							break
					}
					break
				case 1: // mouse event
					def mEvent = mouseEvent.read()
					if(!disable)
					{
						if (mEvent.getID() == MouseEvent.MOUSE_PRESSED) 
						{
							preCon[GET] = true
							def pointValue = mEvent.getPoint()
							point = [(int)pointValue.x, (int)pointValue.y]
						}
					}
					else
					{
						point = [-1000, -1000]
						preCon[GET] = true
					}
					break
			} // end of switch
		} // end while
	} // end run
}
