package c25

import java.io.Serializable;

class GameDetails implements Serializable 
{
	def gameId
	def playerDetails = null
	def pairsSpecification = null
// Update number of players and spectators
	def numberPlayers = null
	def numberSpectators = null
// Contains time till game start - or whether game is underway.
	def gameState = null
	def playerTurn = null
	def updateNumber = null
	def card1Visible = null
	def card2Visible = null
	def timeTillGame = null
	def status = null;
}
