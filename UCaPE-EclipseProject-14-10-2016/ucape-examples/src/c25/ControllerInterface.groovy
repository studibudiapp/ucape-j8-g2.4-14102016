package c25

import java.awt.*
import org.jcsp.awt.*
import org.jcsp.lang.*
import org.jcsp.util.*
import org.jcsp.groovy.*

class ControllerInterface implements CSProcess{
	ActiveCanvas gameCanvas
	ChannelInput IPlabelConfig
	ChannelInput statusConfig
	ChannelInput pairsConfig
	ChannelInputList playerNames
	ChannelInputList pairsWon
// Add channels to update the game status, number of players, and number of spectators
	ChannelInput gameStatusInput
	ChannelInput gamePlayingInput
	ChannelInput gameSpectatorsInput
	
	void run(){
		def root = new ActiveClosingFrame("PAIRS (Turn Over Game) - Main Controller")
		def mainFrame = root.getActiveFrame()
		mainFrame.setSize(900, 800)
		def statusLabel = new ActiveLabel(statusConfig)
		def pairsLabel = new ActiveLabel (pairsConfig)
		def IPLabel = new ActiveLabel (IPlabelConfig)
		pairsLabel.setAlignment(Label.LEFT)
		gameCanvas.setSize(560, 560)

		def buttonContainer = new Container()
		buttonContainer.setLayout(new GridLayout (1,5))
		buttonContainer.add(IPLabel)
		buttonContainer.add(new Label("STATUS"))
		buttonContainer.add(statusLabel)
		buttonContainer.add(new Label("Pairs Unclaimed"))
		buttonContainer.add(pairsLabel)
		
		def outcomeContainer = new Container()
		def maxPlayers = playerNames.size()
		def playerNameSpaces = []
		def playerWonSpaces = []
		for ( i in 0..<maxPlayers) {
			playerNameSpaces << new ActiveLabel (playerNames[i], "Player " + i)
			playerWonSpaces << new ActiveLabel (pairsWon[i], "  ")
		}
// Increased the grid size by one to hold 3 extra labels to the original
		outcomeContainer.setLayout(new GridLayout(4+maxPlayers,2))
		
// Define the game status labels
		def gameStatusLabel = new Label("Game status:                                 ")
		def gameStatus = new ActiveLabel (gameStatusInput, "Not connected")
		def gamePlayingLabel = new Label("Playing: ")
		def gamePlaying = new ActiveLabel (gamePlayingInput, "-")
		def gameSpectatorsLabel = new Label("Spectating: ")
		def gameSpectators = new ActiveLabel (gameSpectatorsInput, "-")
		outcomeContainer.add(gameStatusLabel)
		outcomeContainer.add(gameStatus)
		outcomeContainer.add(gamePlayingLabel)
		outcomeContainer.add(gameSpectatorsLabel)
		outcomeContainer.add(gamePlaying)
		outcomeContainer.add(gameSpectators)
		
		def nameLabel = new Label("Player Name")
		def wonLabel = new Label ("Pairs Won")
		
		outcomeContainer.add(nameLabel)
		outcomeContainer.add(wonLabel)
		
		for ( i in 0 ..< maxPlayers){
			outcomeContainer.add(playerNameSpaces[i])
			outcomeContainer.add(playerWonSpaces[i])
		}
		
		mainFrame.setLayout(new BorderLayout())
		mainFrame.add(gameCanvas, BorderLayout.CENTER)
		mainFrame.add(buttonContainer, BorderLayout.NORTH)
		mainFrame.add(outcomeContainer, BorderLayout.EAST)
		
		mainFrame.pack()
		mainFrame.setVisible(true)	
// Update network to include the new active labels
		def network = [root, gameCanvas, statusLabel, pairsLabel, IPLabel, gameStatus, gamePlaying, gameSpectators]
		network = network + playerNameSpaces + playerWonSpaces
		new PAR(network).run()
	}

}
