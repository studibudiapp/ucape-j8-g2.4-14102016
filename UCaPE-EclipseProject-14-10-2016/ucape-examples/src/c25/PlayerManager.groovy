package c25

import org.jcsp.awt.*
import org.jcsp.groovy.*
import org.jcsp.lang.*
import java.awt.*
import java.awt.Color.*
import org.jcsp.net2.*;
import org.jcsp.net2.tcpip.*;
import org.jcsp.net2.mobile.*;
import java.awt.event.*

class PlayerManager implements CSProcess {
	DisplayList dList
	ChannelOutputList playerNames
	ChannelOutputList pairsWon
	ChannelOutput IPlabel
	ChannelInput IPfield
	ChannelOutput IPconfig
	ChannelInput withdrawButton
	ChannelInput nextButton
	ChannelOutput getValidPoint
	ChannelInput validPoint
	ChannelOutput nextPairConfig
// Add channels to output the game status, number of players, and number of spectators
	ChannelOutput gameStatusOutput
	ChannelOutput gamePlayingOutput
	ChannelOutput gameSpectatorsOutput
	
	int maxPlayers = 8
	int side = 50
	int minPairs = 3
	int maxPairs = 6
	int boardSize = 6
	
// Define the global variables used by this class, so as to easily simplify the code by turning it into more discrete methods.
	def createBoard
	GraphicsCommand[] display
	def changePairs
	int gap
	def chosenPairs
	def enroled
	def currentPair
	def notMatched
	def pairsMap
	def outerAlt
	def innerAlt
	def NEXT
	def VALIDPOINT
	def WITHDRAW
	def toController
	def pairsMatch
	def myPlayerId
	def gameId
	def playerName
	def enrolDetails
	def fromController
	def updateNumber
	def oldTimeTillGameStarts = -1
	def status
	
	void run()
	{
		SetUpGraphics()
		SetUpGame()
		SetUpConnection()
		def unclaimedPairs = 0
		PlayGame()
	} // end run
	
	void PlayGame()
	{
		outerAlt = new ALT([validPoint, withdrawButton])
		def spectatorAlt = new ALT([withdrawButton, new Skip()])
		NEXT = 0
		VALIDPOINT = 0
		WITHDRAW = 1
		if(status == 1)
		{
			IPlabel.write("Hi " + playerName + ", you are now enroled in the PAIRS game")
			IPconfig.write(" ")
		}
		else
		{
			IPlabel.write("Sorry " + playerName + ", " + enrolDetails.errorMessage)
			IPconfig.write("  Feel free to watch in spectator mode :).")
		}
		def gameUpdateNumber = -1
		while (enroled)
		{
			Thread.sleep(150)
			toController.write(new GetGameDetails(id: myPlayerId))
			def gameDetails = (GameDetails)fromController.read()
			if(gameDetails.playerTurn==-1)
			{
				if(oldTimeTillGameStarts != (int) gameDetails.timeTillGame)
				{
					oldTimeTillGameStarts = gameDetails.timeTillGame
					gameStatusOutput.write(gameDetails.gameState)
				}
			}
			if(gameUpdateNumber!=gameDetails.updateNumber)
			{
				gameUpdateNumber=gameDetails.updateNumber
				chosenPairs = [null, null]
				createBoard()
				dList.change (display, 0)
				gameId = gameDetails.gameId
				status = gameDetails.status
				if(gameDetails.playerTurn==-1)
				{
					IPconfig.write("  Waiting for players to join")
				}
				else
				{
					if(status==0)
					{
						IPconfig.write("  You are watching game number - " + gameId)
						IPlabel.write(" ")
					}
					else
					{
						IPconfig.write("  Playing Game Number - " + gameId)
						IPlabel.write("Hi " + playerName + ", you are playing in the PAIRS game")
					}
				}
				gamePlayingOutput.write(gameDetails.numberPlayers+"")
				gameSpectatorsOutput.write(gameDetails.numberSpectators+"")
				def playerMap = gameDetails.playerDetails
				pairsMap = gameDetails.pairsSpecification
				def playerIds = playerMap.keySet()
				playerIds.each
				{
					p ->
					def pData = playerMap.get(p)
					playerNames[p].write(pData[0])
					pairsWon[p].write(" " + pData[1])
				}
				// now use pairsMap to create the board
				def pairLocs = pairsMap.keySet()
				pairLocs.each
				{
					loc ->
					changePairs(loc[0], loc[1], Color.LIGHT_GRAY, -1)
				}
				currentPair = 0
				notMatched = true
				if(gameDetails.playerTurn==myPlayerId)
				{
					gameStatusOutput.write("It is your turn, " + playerName.trim() + "!")
					EnableMouseInput()
					PlayTurn()
					DisableMouseInput()
				}
				else 
				{
					gameStatusOutput.write(gameDetails.gameState)
					if(gameDetails.card1Visible[0]>-1)
					{
						def pair1Data = pairsMap.get(gameDetails.card1Visible)
						changePairs(gameDetails.card1Visible[0], gameDetails.card1Visible[1], pair1Data[1], pair1Data[0])
						if(gameDetails.card2Visible[0]>-1)
						{
							def pair2Data = pairsMap.get(gameDetails.card2Visible)
							changePairs(gameDetails.card2Visible[0], gameDetails.card2Visible[1], pair2Data[1], pair2Data[0])
							Thread.sleep(1000)
						}
					}
					switch ( spectatorAlt.priSelect() )
					{
						case 0: // WITHDRAW BUTTON
							withdrawButton.read()
							if(status == 1)
							{
								toController.write(new WithdrawFromGame(id: myPlayerId))
								status == 0
								IPlabel.write("You have left the game and are now in spectator Mode")
								IPconfig.write("  You are watching game number - " + gameId)
							}
							break
						case 1: // skip
							break;
					}
				}
			}
		}
		IPlabel.write("Goodbye " + playerName + ", please close game window")
	}
	
	void EnableMouseInput()
	{
		getValidPoint.write("enable")
	}
	void DisableMouseInput()
	{
		getValidPoint.write("disable")
	}
	
	void PlayTurn()
	{
		while ((chosenPairs[1] == null) && (enroled) && (notMatched) && status == 1)
		{
			
			getValidPoint.write (new GetValidPoint( side: side,
													gap: gap,
													pairsMap: pairsMap))
			switch ( outerAlt.select() )
			{
				case WITHDRAW:
					withdrawButton.read()
					if(status == 1)
					{
						toController.write(new WithdrawFromGame(id: myPlayerId))
						status == 0
						IPlabel.write("You have left the game and are now in spectator Mode")
						IPconfig.write("  You are watching game number - " + gameId)
					}
					break
				case VALIDPOINT:
					def vPoint = ((SquareCoords)validPoint.read()).location
					chosenPairs[currentPair] = vPoint
					currentPair = currentPair + 1
					if(currentPair == 2)
					{
						if(vPoint[0] == chosenPairs[0][0] && vPoint[1] ==chosenPairs[0][1])
						{
							chosenPairs[1] = null
							currentPair = 1
							break
						}
					}
					def pairData = pairsMap.get(vPoint)
					changePairs(vPoint[0], vPoint[1], pairData[1], pairData[0])
					def matchOutcome = pairsMatch(pairsMap, chosenPairs)
					if ( matchOutcome == 2)  
					{
						ShowTurnedCards()
						Thread.sleep(800)
						ClearTurnedCards()
						EndTurn()
					} 
					else if ( matchOutcome == 1) 
					{
						ShowTurnedCards()
						Thread.sleep(800)
						ClearTurnedCards()
						notMatched = false
						toController.write(new ClaimPair ( id: myPlayerId,
																 gameId: gameId,
														   p1: chosenPairs[0],
														   p2: chosenPairs[1]))
						gameStatusOutput.write("You got a pair! Play again!!")
						Thread.sleep(500)
					}
					else
					{
						ShowTurnedCards()
					}
					break
			}// end of outer switch
		} // end of while getting two pairs
	}
	
	void ShowTurnedCards()
	{
		if(currentPair == 1)
		{
			toController.write(new TurnCard ( id: myPlayerId,
				gameId: gameId,
				p1: chosenPairs[0],
				p2: [-1,-1]))
		}
		else if(currentPair == 2)
		{
			toController.write(new TurnCard ( id: myPlayerId,
				gameId: gameId,
				p1: chosenPairs[0],
				p2: chosenPairs[1]))
		}
	}
	
	void ClearTurnedCards()
	{
		toController.write(new TurnCard ( id: myPlayerId,
			gameId: gameId,
			p1: [-1,-1],
			p2: [-1,-1]))
	}
	
	void EndTurn()
	{
		toController.write(new FinishTurn ( id: myPlayerId,
			gameId: gameId))
	}
	
	void SpectateGame()
	{
		IPlabel.write("Sorry " + playerName + ", " + enrolDetails.errorMessage)
		IPconfig.write("  Feel free to watch in spectator mode :).")
	}
	
	void SetUpGraphics()
	{
		gap = 5
		def offset = [gap, gap]
		int graphicsPos = (side / 2)
		def rectSize = ((side+gap) *boardSize) + gap
		
		def displaySize = 4 + (5 * boardSize * boardSize)
		display = new GraphicsCommand[displaySize]
		GraphicsCommand[] changeGraphics = new GraphicsCommand[5]
		changeGraphics[0] = new GraphicsCommand.SetColor(Color.WHITE)
		changeGraphics[1] = new GraphicsCommand.FillRect(0, 0, 0, 0)
		changeGraphics[2] = new GraphicsCommand.SetColor(Color.BLACK)
		changeGraphics[3] = new GraphicsCommand.DrawRect(0, 0, 0, 0)
		changeGraphics[4] = new GraphicsCommand.DrawString("   ",graphicsPos,graphicsPos)

		createBoard = 
		{
			display[0] = new GraphicsCommand.SetColor(Color.WHITE)
			display[1] = new GraphicsCommand.FillRect(0, 0, rectSize, rectSize)
			display[2] = new GraphicsCommand.SetColor(Color.BLACK)
			display[3] = new GraphicsCommand.DrawRect(0, 0, rectSize, rectSize)
			def cg = 4
			for ( x in 0..(boardSize-1)){
				for ( y in 0..(boardSize-1)){
					def int xPos = offset[0]+(gap*x)+ (side*x)
					def int yPos = offset[1]+(gap*y)+ (side*y)
					//print " $x, $y, $xPos, $yPos, $cg, "
					display[cg] = new GraphicsCommand.SetColor(Color.WHITE)
					cg = cg+1
					display[cg] = new GraphicsCommand.FillRect(xPos, yPos, side, side)
					cg = cg+1
					display[cg] = new GraphicsCommand.SetColor(Color.BLACK)
					cg = cg+1
					display[cg] = new GraphicsCommand.DrawRect(xPos, yPos, side, side)
					cg = cg+1
					xPos = xPos + graphicsPos
					yPos = yPos + graphicsPos
					display[cg] = new GraphicsCommand.DrawString("   ",xPos, yPos)
					//println "$cg"
					cg = cg+1
				}
			}
		} // end createBoard
		
		def pairLocations = []
		def colours = [Color.MAGENTA, Color.CYAN, Color.YELLOW, Color.PINK]
		
		changePairs = {x, y, colour, p ->
			def int xPos = offset[0]+(gap*x)+ (side*x)
			def int yPos = offset[1]+(gap*y)+ (side*y)
			changeGraphics[0] = new GraphicsCommand.SetColor(colour)
			changeGraphics[1] = new GraphicsCommand.FillRect(xPos, yPos, side, side)
			changeGraphics[2] = new GraphicsCommand.SetColor(Color.BLACK)
			changeGraphics[3] = new GraphicsCommand.DrawRect(xPos, yPos, side, side)
			xPos = xPos + graphicsPos
			yPos = yPos + graphicsPos
			if ( p >= 0)
				changeGraphics[4] = new GraphicsCommand.DrawString("   " + p, xPos, yPos)
			else
				changeGraphics[4] = new GraphicsCommand.DrawString(" ??", xPos, yPos)
			dList.change(changeGraphics, 4 + (x*5*boardSize) + (y*5))
		}
	}
	
	void SetUpGame()
	{
		pairsMatch = {pairsMap, cp ->
			// cp is a list comprising two elements each of which is a list with the [x,y]
			// location of a square
			// returns 0 if only one square has been chosen so far
			//         1 if the two chosen squares have the same value (and colour)
			//         2 if the chosen squares have different values
			if (cp[1] == null) return 0
			else {
				if (cp[0] != cp[1]) {
					def p1Data = pairsMap.get(cp[0])
					def p2Data = pairsMap.get(cp[1])
					if (p1Data[0] == p2Data[0]) return 1 else return 2
				}
				else  return 2
			}
		}
		createBoard()
		dList.set(display)
	}
	
	void SetUpConnection()
	{
		IPlabel.write("What is your name?")
		playerName = IPfield.read()
		IPconfig.write(" ")
		IPlabel.write("What is the IP address of the game controller?")
		def controllerIP = IPfield.read().trim()
		IPconfig.write(" ")
		IPlabel.write("Connecting to the GameController")
		// create Node and Net Channel Addresses
		def nodeAddr = new TCPIPNodeAddress (4000)
		Node.getInstance().init (nodeAddr)
		def toControllerAddr = new TCPIPNodeAddress ( controllerIP, 3000)
		toController = NetChannel.any2net(toControllerAddr, 50 )
		fromController = NetChannel.net2one()
		def fromControllerLoc = fromController.getLocation()
		
		// connect to game controller
		IPconfig.write("Now Connected - sending your name to Controller")
		def enrolPlayer = new EnrolPlayer( name: playerName,
										   toPlayerChannelLocation: fromControllerLoc)
		toController.write(enrolPlayer)
		enrolDetails = (EnrolDetails)fromController.read()
// test and update game status
		gameStatusOutput.write("Connected!")
		gamePlayingOutput.write(enrolDetails.players+"")
		gameSpectatorsOutput.write(enrolDetails.spectators+"")
		status = (int) enrolDetails.status
		myPlayerId = enrolDetails.id
		enroled = true
		Thread.sleep(500)
	}
}				
