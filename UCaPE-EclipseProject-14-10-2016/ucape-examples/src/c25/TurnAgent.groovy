package c25

import org.jcsp.lang.*
import org.jcsp.groovy.MobileAgent

class TurnAgent implements MobileAgent {
  
	// Holds a list of active players
	// Clears mouse buffer upon arrival
	// Activates player's turn upon arrival
	// Notifies player that turn has begun
	// Notifies server when a card is turned, and when a pair is achieved
	// Holds a record of which player it's at
	// Sends itself to next player once turn is finished if the game isn't finished yet.
	
  def ChannelOutput toLocal
  def ChannelInput fromLocal
  def players = [ ]
  def currentPlayer = 0
                  
  def connect ( c ) {
    this.toLocal = c[0]
    this.fromLocal = c[1]
  }
  
  def disconnect () {
    toLocal = null
    fromLocal = null
  }
  
  void run() {
	toLocal.write("clear mouse buffer")
	def n=1
	while(n>0)
	{
		toLocal.write("take turn")
		def pair = fromLocal.read()
		if(pair == true)
		{
			n=2
		}
	}
	if(currentPlayer+1==players.size)
	{
		currentPlayer = 0
	}
	else
	{
		currentPlayer++
	}
	toLocal.write(players[currentPlayer])
  }
}