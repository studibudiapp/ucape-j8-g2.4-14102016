package c25

import org.jcsp.awt.*
import org.jcsp.lang.*
import org.jcsp.util.*
import org.jcsp.groovy.*
import java.awt.*
import java.awt.Color.*
import java.util.concurrent.ThreadPoolExecutor.AbortPolicy

import org.jcsp.net2.*;
import org.jcsp.net2.tcpip.*;
import org.jcsp.net2.mobile.*;

// modify this so that the controller asks the min and max number of pairs
// then generates them randomly
// it should automatically create a new game when all the pairs have been claimed
// this should be done with a random number of pairs
// that is the interaction to determine the number of pairs should be removed.
// when a person enrolls they are given the current state of the game 
// which they can then join

class ControllerManager implements CSProcess
{
	DisplayList dList
	ChannelOutput IPlabelConfig
	ChannelOutput statusConfig
	ChannelOutput pairsConfig
	ChannelOutputList playerNames
	ChannelOutputList pairsWon
// Add channels to output the game status, number of players, and number of spectators
	ChannelOutput gameStatusOutput
	ChannelOutput gamePlayingOutput
	ChannelOutput gameSpectatorsOutput
	
	int maxPlayers = 8
	int side = 50
	int minPairs = 6
	int maxPairs = 18
	int boardSize = 6
	
// Define the global variables used by this class, so as to easily simplify the code by turning it into more discrete methods.
	def availablePlayerIds
	def timer
	def timeout
	def totalPlayers
	def totalSpectators
	def currentGameStatus
	def totalPlayersConnected
	def currentPlayerId
	def playerMap
	def toPlayers
	def pairsMap
	def gameId
	def running
	def pairsUnclaimed
	GraphicsCommand[] display
	GraphicsCommand[] changeGraphics
	def offset
	int graphicsPos
	def int gap
	def createBoard
	def createPairs
	int pairsRange
	def updateNumber
	def card1Visible = [-1,-1]
	def card2Visible = [-1,-1]
	def playerTurn = -1
	def oldTimeTillGameStarts = 0;
	def GameStartDelay = 15 // define game start in seconds
	def playingArray = [] // This holds which players are playing, and which ones are spectating
	
	void run()
	{
		SetupGraphics()
		
		// create a Node and the fromPlayers net channel
		// def ipOfController = "192.168.43.86"
		def ipOfController = "192.168.1.64"
		// def ipOfController = "192.168.43.86"
		def nodeAddr = new TCPIPNodeAddress (ipOfController, 3000)
		Node.getInstance().init (nodeAddr)
		IPlabelConfig.write(nodeAddr.getIpAddress())
		//println "Controller IP address = ${nodeAddr.getIpAddress()}"
		
		def fromPlayers = NetChannel.net2one()
		def fromPlayersLoc = fromPlayers.getLocation()
		//println "Controller: fromPlayer channel location - ${fromPlayersLoc.toString()}"
		
// Define a timer instance to set when the game starts
		timer = new CSTimer()
		timeout = timer.read()           // set current time
// Store the current number of players and spectators
		totalPlayers = 0
		totalSpectators = 0
		
		toPlayers = new ChannelOutputList()
		for ( p in 0..<maxPlayers) toPlayers.append(null)
		currentPlayerId = 0
		playerMap = [:]
		
		createBoard()		
		dList.set(display)
		def nPairs = 0
		pairsUnclaimed = 0
		gameId = 0
		
// test and update game status
		gameStatusOutput.write("Waiting for players to join")
		gamePlayingOutput.write("0")
		gameSpectatorsOutput.write("0")
		while (true) 
		{
			statusConfig.write("Creating")
			//	nPairs = generatePairsNumber(minPairs, pairsRange)
			nPairs = maxPairs
			pairsUnclaimed = nPairs
			pairsConfig.write(" "+ nPairs)
			gameId = gameId + 1
			createPairs (nPairs)
			statusConfig.write("Running")
			running = (pairsUnclaimed != 0)
// Create a counter for the number of enrolled players
			totalPlayersConnected = 0;
			updateNumber = 0
			while (running)
			{
				if(totalPlayersConnected>0 && timer.read()>timeout)
				{
					currentGameStatus = ""
					if(playerTurn==-1)
					{
						playerTurn = 0
						updateNumber++
					}
					currentGameStatus = "It is " + playerMap.get(playerTurn)[0].trim() + "'s turn"
					gameStatusOutput.write(currentGameStatus)
				}
				else if(totalPlayersConnected>0)
				{
					def timeTillGameStarts = (int) ((timeout-timer.read())/1000)
					if(oldTimeTillGameStarts != (int) timeTillGameStarts)
					{
						oldTimeTillGameStarts = (int) timeTillGameStarts
						currentGameStatus = "Game starting in " + timeTillGameStarts + " seconds ..."
						gameStatusOutput.write(currentGameStatus)
					}
				}
				else
				{
					currentGameStatus = "Waiting for players to join"
				}
				CSTimer updateTimer = new CSTimer ();
				Alternative alt = new Alternative (fromPlayers, updateTimer);
				updateTimer.setAlarm (updateTimer.read () + 250);
				switch(alt.priSelect ())
				{
					case 1:
						updateTimer.setAlarm (updateTimer.read () + 250);
					break;
					case 0:
						def o = fromPlayers.read()
						if ( o instanceof EnrolPlayer) 
						{
							EnrolNewPlayer(o)
							updateNumber++
						} 
						else if ( o instanceof GetGameDetails) 
						{
							UpdatePlayer(o)
						} 
						else if ( o instanceof ClaimPair) 
						{
							ClaimNewPair(o)
							updateNumber++
						} 
						else if ( o instanceof TurnCard)
						{
							card1Visible = ( (TurnCard) o).p1
							card2Visible = ( (TurnCard) o).p2
							updateNumber++
						}
						else if ( o instanceof FinishTurn)
						{
							FinishTurn(( (FinishTurn) o).id);
						}
						else 
						{
							WithdrawPlayer(o)
							updateNumber++
						} // end else if chain
					break;
				}
			} // while running
			// Allow spectators to join
			for(int i=0;i<playingArray.size();i++)
			{
				if(playingArray[i]==0)
				{
					totalPlayers++
					totalSpectators--
					playingArray[i]=1
				}
						
			}
			createBoard()
			dList.change(display, 0)	
		} // end while true		
	} // end run
	
	void FinishTurn(id)
	{
		card1Visible = [-1,-1]
		card2Visible = [-1,-1]
		def doWhileCondition = true
		while(doWhileCondition)
		{
			if(playerTurn>playingArray.size()-2)
			{
				playerTurn = 0
			}
			else
			{
				playerTurn++
			}
			doWhileCondition = playingArray[playerTurn]==0
		}
		println "Player number: " + playerTurn
		updateNumber++
	}
	
	/**
	 * Take the enrol player process and turn it into a separate method to simplify logic and modifications
	 */
	
	void EnrolNewPlayer(o)
	{
		if(totalPlayersConnected==0)
		{
			timeout = timer.read() + (GameStartDelay * 1000)
		}
		totalPlayersConnected++;
		def playerDetails = (EnrolPlayer)o
		//println "name: ${playerDetails.name}"
// Also if the player joined too late - they are forced to join in spectator mode
		if (availablePlayerIds.size() > 0 && timer.read()<timeout)
		{
			// Player is able to play
			playingArray[totalPlayersConnected-1] = 1
			totalPlayers++;
			Enrol(playerDetails, " ")
		}
		else
		{
			// Player is able to watch
			playingArray[totalPlayersConnected-1] = 0
			totalSpectators++;
			// Vary response based on error
			if(availablePlayerIds.size() >= 0)
			{
				Enrol(playerDetails, "this game has already begun :( ")
			}
			else
			{
				Enrol(playerDetails, "there are too many players enroled in this PAIRS game")
			}
			//EnrolAsSpectator(playerDetails)
		}
	}
	
	/**
	 * This covers the logic for when a new player joins
	 * @param playerDetails
	 */
	
	void Enrol(playerDetails, msg)
	{
		def playerName = playerDetails.name
		def playerToAddr = playerDetails.toPlayerChannelLocation
		def playerToChan = NetChannel.one2net(playerToAddr)
		gamePlayingOutput.write(totalPlayers+"")
		def overList = false
		currentPlayerId = availablePlayerIds. pop()
		playerNames[currentPlayerId].write(playerName)
		pairsWon[currentPlayerId].write(" " + 0)
		toPlayers[currentPlayerId] = playerToChan
		println playingArray[currentPlayerId] + ", " + currentPlayerId
		toPlayers[currentPlayerId].write(new EnrolDetails(id: currentPlayerId, players: totalPlayers, spectators: totalSpectators, status: playingArray[currentPlayerId], errorMessage: msg))
		playerMap.put(currentPlayerId, [playerName, 0]) // [name, pairs claimed]
	}
	
	/**
	 * This covers the logic for when a new spectator joins
	 * @param playerDetails
	 */
	
	void EnrolAsSpectator(playerDetails)
	{
		def playerName = playerDetails.name
		def playerToAddr = playerDetails.toPlayerChannelLocation
		def playerToChan = NetChannel.one2net(playerToAddr)
// add to spectators list
		totalSpectators++;
		gameSpectatorsOutput.write(totalSpectators+"")
		// no new players can join the game
// Vary response based on error
		if(availablePlayerIds.size() <= 0)
		{
			playerToChan.write(new EnrolDetails(id: -1,players: totalPlayers, spectators: totalSpectators))
		}
		else
		{
			playerToChan.write(new EnrolDetails(id: -1, errorMessage: "this game has already begun :( ", players: totalPlayers, spectators: totalSpectators))
		}
	}
	
	/**
	 * Take the update player process and turn it into a separate method to simplify the update process
	 */
	
	void UpdatePlayer(o)
	{
		def ggd = (GetGameDetails)o
		def id = ggd.id
		// update the player with the latest details
		toPlayers[id].write(new GameDetails( playerDetails: playerMap,
											 pairsSpecification: pairsMap,
											 gameId: gameId,
											 numberPlayers: totalPlayers,
											 numberSpectators: totalSpectators,
											 gameState: currentGameStatus,
											 playerTurn: playerTurn,
											 updateNumber: updateNumber,
											 card1Visible: card1Visible,
											 card2Visible: card2Visible,
											 timeTillGame: (int) ((timeout-timer.read())/1000),
											 status:playingArray[id]))
		// update controller
	}
	
	void ClaimNewPair(o)
	{
		def claimPair = (ClaimPair)o
		def gameNo = claimPair.gameId
		def id = claimPair.id
		def p1 = claimPair.p1
		def p2 = claimPair.p2
		if ( gameId == gameNo){
			if ((pairsMap.get(p1) != null) ) {
				// pair can be claimed
				//println "before remove of $p1, $p2"
				//pairsMap.each {println "$it"}
				pairsMap.remove(p2)
				pairsMap.remove(p1)
				//println "after remove of $p1, $p2"
				//pairsMap.each {println "$it"}
				def playerState = playerMap.get(id)
				playerState[1] = playerState[1] + 1
				pairsWon[id].write(" " + playerState[1])
				playerMap.put(id, playerState)
				pairsUnclaimed = pairsUnclaimed - 1
				pairsConfig.write(" "+ pairsUnclaimed)
				running = (pairsUnclaimed != 0)
			}
			else {
				//println "cannot claim pair: $p1, $p2"
			}
		}
	}
	
	void WithdrawPlayer(o)
	{
		def withdraw = (WithdrawFromGame)o
		def id = withdraw.id
		if(playingArray[id]==1)
		{
			//def playerState = playerMap.get(id)
			//println "Player: ${playerState[0]} claimed ${playerState[1]} pairs"
			//playerNames[id].write("       ")
			//pairsWon[id].write("   ")
			//toPlayers[id] = null
			//availablePlayerIds << id
			//availablePlayerIds =  availablePlayerIds.sort().reverse()
			println id
			playingArray[id]=0
			println "removing player " + id
			totalPlayers--
			totalSpectators++
			if(playerTurn==id)
			{
				FinishTurn(id)
			}
		}
	}
	
	void SetupGraphics()
	{
		gap = 5
		offset = [gap, gap]
		graphicsPos = (side / 2)
		
		availablePlayerIds = ((maxPlayers-1) .. 0).collect{it}
		
		// println "$availablePlayerIds"
		def generatePairsNumber =
		{ min, range ->
			def rng = new Random()
			def randomAmount = rng.nextInt(range)
			return min + randomAmount
		}
		def displaySize = 4 + (5 * boardSize * boardSize)
		display = new GraphicsCommand[displaySize]
		changeGraphics = new GraphicsCommand[5]
		changeGraphics[0] = new GraphicsCommand.SetColor(Color.WHITE)
		changeGraphics[1] = new GraphicsCommand.FillRect(0, 0, 0, 0)
		changeGraphics[2] = new GraphicsCommand.SetColor(Color.BLACK)
		changeGraphics[3] = new GraphicsCommand.DrawRect(0, 0, 0, 0)
		changeGraphics[4] = new GraphicsCommand.DrawString("   ",graphicsPos,graphicsPos)

		createBoard = CreateNewBoard()
		pairsMap =[:]
		
		createPairs = CreateAllPairs()
	}
	
	
	def CreateNewBoard()
	{
		def rectSize = ((side+gap) *boardSize) + gap
		pairsRange = maxPairs - minPairs
		
		return {
			display[0] = new GraphicsCommand.SetColor(Color.WHITE)
			display[1] = new GraphicsCommand.FillRect(0, 0, rectSize, rectSize)
			display[2] = new GraphicsCommand.SetColor(Color.BLACK)
			display[3] = new GraphicsCommand.DrawRect(0, 0, rectSize, rectSize)
			def cg = 4
			for ( x in 0..(boardSize-1))
			{
				for ( y in 0..(boardSize-1))
				{
					def int xPos = offset[0]+(gap*x)+ (side*x)
					def int yPos = offset[1]+(gap*y)+ (side*y)
					//print " $x, $y, $xPos, $yPos, $cg, "
					display[cg] = new GraphicsCommand.SetColor(Color.WHITE)
					cg = cg+1
					display[cg] = new GraphicsCommand.FillRect(xPos, yPos, side, side)
					cg = cg+1
					display[cg] = new GraphicsCommand.SetColor(Color.BLACK)
					cg = cg+1
					display[cg] = new GraphicsCommand.DrawRect(xPos, yPos, side, side)
					cg = cg+1
					xPos = xPos + graphicsPos
					yPos = yPos + graphicsPos
					display[cg] = new GraphicsCommand.DrawString("   ",xPos, yPos)
					// println "$cg"
					cg = cg+1
				}
			}
		} // end createBoard
	}
	
	def CreateAllPairs()
	{
		def changePairs =
		{x, y, colour, p ->
			def int xPos = offset[0]+(gap*x)+ (side*x)
			def int yPos = offset[1]+(gap*y)+ (side*y)
			changeGraphics[0] = new GraphicsCommand.SetColor(colour)
			changeGraphics[1] = new GraphicsCommand.FillRect(xPos, yPos, side, side)
			changeGraphics[2] = new GraphicsCommand.SetColor(Color.BLACK)
			changeGraphics[3] = new GraphicsCommand.DrawRect(xPos, yPos, side, side)
			xPos = xPos + graphicsPos
			yPos = yPos + graphicsPos
			if ( p > -1)
				changeGraphics[4] = new GraphicsCommand.DrawString(" " + p, xPos, yPos)
			else
				changeGraphics[4] = new GraphicsCommand.DrawString("   ", xPos, yPos)
		}
		
		def colours = [Color.MAGENTA, Color.CYAN, Color.YELLOW, Color.PINK]
		
		def initPairsMap =
		{
			for ( x in 0 ..< boardSize)
			{
				for ( y in 0 ..< boardSize)
				{
					pairsMap.put([x,y], null)
				}
			}
		}
		
		return {np ->
			//println "createpairs: $np"
			/*
			 * have to check that all locations are distinct
			 * that is pairs map does not already contain a location that
			 * is already in use
			 */
			def rng = new Random()
			initPairsMap()
			for (p in 1..np)
			{
				def x1 = rng.nextInt(boardSize)
				def y1 = rng.nextInt(boardSize)
				//println "[x1, y1] = [$x1, $y1]"
				while ( pairsMap.get([x1,y1]) != null)
				{
					//println "first repeated random location [$x1, $y1]"
					x1 = rng.nextInt(boardSize)
					y1 = rng.nextInt(boardSize)
				}
				pairsMap.put([x1, y1], [p, colours[p%4]])
				changePairs(x1, y1, colours[p%4], p)
				dList.change(changeGraphics, 4 + (x1*5*boardSize) + (y1*5))
				def x2 = rng.nextInt(boardSize)
				def y2 = rng.nextInt(boardSize)
				//println "[x2, y2] = [$x2, $y2]"
				while ( pairsMap.get([x2,y2]) != null)
				{
					//println "second repeated random location [$x2, $y2]"
					x2 = rng.nextInt(boardSize)
					y2 = rng.nextInt(boardSize)
				}
				//println "final pairs: [$x1, $y1], [$x2, $y2] for $p"
				pairsMap.put([x2, y2], [p, colours[p%4]])
				changePairs(x2, y2, colours[p%4], p)
				dList.change(changeGraphics, 4 + (x2*5*boardSize) + (y2*5))
			}
		} // end createPairs
	}
	
}
/**

while (true)
{
  timeout = timeout + testInterval           // set the timeout
  timer.after ( timeout )                    // wait for the timeout
  suspend.write (0)                          // suspend signal to ScaleInt; value irrelevant
  currentFactor = factor.read()              // get current scaling from ScaleInt
  currentFactor = currentFactor + addition   // compute new factor
  timer.sleep(computeInterval)               // to simulate computational time
  injector.write ( currentFactor )          // send new scale factor to Scale
}

*/