package c09;
   
// copyright 2012-13 Jon Kerridge
// Let's Do It In Parallel
import org.jcsp.lang.*
import org.jcsp.groovy.*
import org.jcsp.groovy.plugAndPlay.*
import org.jcsp.groovy.util.*

class EventProcessing implements CSProcess
{ 
	 
  def ChannelInputList eventStreams
  def minTime = 500
  def maxTime = 750
  
  void run() 
  {
    def mux2udd = Channel.one2one()
    def udd2sum = Channel.one2one()
    def sum2prn = Channel.one2one()        
    def pList = [  
//                   new FairMultiplex ( inChannels: eventStreams,
//                                        outChannel: mux2udd.out() ),
/*                   
                   new PriMultiplex ( inChannels: eventStreams,
                                      outChannel: mux2udd.out() ),                     
*/                   

                   
                   new Multiplexer ( inChannels: eventStreams,
                                     outChannel: mux2udd.out() ),                     

                   new UniformlyDistributedDelay ( inChannel:mux2udd.in(), 
                                                   outChannel: udd2sum.out(), 
                                                   minTime: minTime, 
                                                   maxTime: maxTime ),                     
                   new DataPacketSummary ( inChannel:udd2sum.in(), 
                                           outChannel: sum2prn.out(),
                                           ChannelQuantity: eventStreams.size() ), 
											   
                   new GPrint ( inChannel: sum2prn.in(),
         		                 heading : "Event Output",
         		                 delay: 0)                    
                 ]
    new PAR (pList).run()
  }
}