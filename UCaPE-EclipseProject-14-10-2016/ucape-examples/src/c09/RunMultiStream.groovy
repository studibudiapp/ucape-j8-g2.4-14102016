package c09
 
// copyright 2012-13 Jon Kerridge
// Let's Do It In Parallel
import org.jcsp.lang.*
import org.jcsp.groovy.*
import org.jcsp.groovy.plugAndPlay.*
import phw.util.Ask

def sources = Ask.Int ("Number of event sources between 1 and 9 ? ", 1, 9)

minTimes = [ 111, 115, 119, 123, 123, 123, 13, 13, 13 ]
maxTimes = [ 112, 116, 120, 124, 124, 124, 114, 114, 114 ]    
      
def es2ep = Channel.one2oneArray(sources)

ChannelInputList eventsList = new ChannelInputList (es2ep)

def sourcesList = ( 0 ..< sources).collect { i ->
            new EventSource ( source: i+1, 
                              outChannel: es2ep[i].out(),
                              minTime: minTimes[i],
                              maxTime: maxTimes[i] ) 
            }

def eventProcess = new EventProcessing ( eventStreams: eventsList,
                                          minTime: 10,
                                          maxTime: 400 )

new PAR( sourcesList + eventProcess).run()
