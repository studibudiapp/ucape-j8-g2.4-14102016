package c09

import org.jcsp.lang.*
import org.jcsp.groovy.*

class TestMissed implements CSProcess
{
	def ChannelInput inChannel
	def ChannelOutput outChannel
	def previousNumber = -1
	
	void run () 
	{	  
	  while (true) {
		EventData data = (EventData) inChannel.read().copy()
		def missedRecord = data.missed
		def actualMissed = data.data-previousNumber-1
		previousNumber = data.data
		def testResults = "TEST RESULTS\nRecorded number of missed events: " + missedRecord +"\nActual number of missed events: " + actualMissed +"\nTest passed: " + (actualMissed==missedRecord) + "\n"
		data.testResults = testResults
		outChannel.write( data )
	  }
	}
}
