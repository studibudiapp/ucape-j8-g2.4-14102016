package c09
	
import org.jcsp.lang.*
import org.jcsp.groovy.*

class DataPacketSummary implements CSProcess
{
	def ChannelInput inChannel
	def ChannelOutput outChannel
	def ChannelQuantity
	def dataSummary
	
	void run ()
	{
		dataSummary = new int[ChannelQuantity]
		while (true) 
		{
			EventData data = (EventData) inChannel.read().copy()
			def channelSource = data.source
			def missed = data.missed
			dataSummary[channelSource-1] += missed
			def testResults = "TEST SUMMARY\nFor "+ ChannelQuantity + " sources.\n"
			for(int i=0; i< dataSummary.length;i++)
			{
				testResults += "Source: " + (i+1) + " missed: " +  dataSummary[i] +".\n"
			}
			data.testResults = testResults
			outChannel.write( data )
		}
	}
}
