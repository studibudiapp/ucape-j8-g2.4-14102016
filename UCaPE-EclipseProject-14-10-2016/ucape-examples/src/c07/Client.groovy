package c07

// copyright 2012-13 Jon Kerridge
// Let's Do It In Parallel

import org.jcsp.lang.*
import org.jcsp.groovy.*


class Client implements CSProcess{  
	
  def ChannelInput receiveChannel
  def ChannelOutput requestChannel
  def clientNumber   
  def selectList = [ ] 
   
  void run () {
    def iterations = selectList.size
	def responsesReceived = 0 		// This is used to check which request the last response should be linked with.
	def allResponsesInOrder = true 	// This is used to test whether any responses arrived out of order
    println "Client $clientNumber has $iterations values in $selectList"
	
    for ( i in 0 ..< iterations) {
      def key = selectList[i]
	  // println "client ${clientNumber} is sending a request for ${selectList[i]} to a server"
      requestChannel.write(key)
      def v = receiveChannel.read()
	  // println "Client $clientNumber received value: ${v} for request number ${selectList[responsesReceived]}"
	  def inOrder = (v == 10* selectList[responsesReceived])
	  // println "Responses are in order: ${inOrder}"
	  if(!inOrder)
	  {
		  allResponsesInOrder = false
	  }
	  responsesReceived++
    }
	if(allResponsesInOrder)
    {
		println "Client $clientNumber has finished and all responses returned in the right order."
    }
	else
	{
		println "Client $clientNumber has finished but the responses did not all return in the right order."
	}
  }
}
