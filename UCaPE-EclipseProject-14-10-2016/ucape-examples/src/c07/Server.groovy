package c07

// copyright 2012-13 Jon Kerridge
// Let's Do It In Parallel

import org.jcsp.lang.*
import org.jcsp.groovy.*


class Server implements CSProcess{
	  
  def ChannelInput clientRequest
  def ChannelOutput clientSend  
  def ChannelOutput thisServerRequest
  def ChannelInput thisServerReceive  
  def ChannelInput otherServerRequest
  def ChannelOutput otherServerSend  
  def dataMap = [ : ]    
  def serverNumber
                
  void run () {
    def CLIENT = 0
    def OTHER_REQUEST = 1
    def THIS_RECEIVE = 2
    def serverAlt = new ALT ([clientRequest, 
		                      otherServerRequest, 
							  thisServerReceive])
    while (true) {
      def index = serverAlt.select()	  
      switch (index) {		  
        case CLIENT :
		  // println "${serverNumber} received client's request"
          def key = clientRequest.read()
          if ( dataMap.containsKey(key) ) 
		  {
			// println "${serverNumber} is returning requested data to client"
            clientSend.write(dataMap[key])          
		  }
          else 
		  {
			// println "${serverNumber} is forwarding client's request to other server"
            thisServerRequest.write(key)
		  }
          //end if 
          break
        case OTHER_REQUEST :
		  // println "${serverNumber} received a request from the other server"
          def key = otherServerRequest.read()
          if ( dataMap.containsKey(key) ) 
		  {
			// println "${serverNumber} is sending requested data back to other server"
            otherServerSend.write(dataMap[key])   
		  }       
          else 
		  {
			// println "${serverNumber} is sending 'data request failed' back to other server"
            otherServerSend.write(-1)
		  }
          //end if 
          break
        case THIS_RECEIVE :
		  // println "${serverNumber} has received a response from the other server"
		  // println "${serverNumber} is returning this to client"
          clientSend.write(thisServerReceive.read() )
          break
      } // end switch              
    } //end while   
  } //end run
}
