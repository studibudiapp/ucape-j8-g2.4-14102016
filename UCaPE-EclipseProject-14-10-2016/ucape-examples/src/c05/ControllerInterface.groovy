package c05

import java.awt.*
	import org.jcsp.awt.*
	import org.jcsp.lang.*
	import org.jcsp.util.*
	import org.jcsp.groovy.*

class ControllerInterface implements CSProcess
{
		  def ChannelInput factor
		  def ChannelOutput suspend
		  def ChannelOutput injector
		
		void run()
		{
			def root = new ActiveClosingFrame("Scaler Controller")
			def mainFrame = root.getActiveFrame()
			mainFrame.setSize(900, 800)
			ActiveButton suspendButton = new ActiveButton(null, suspend, "Suspend Scaling and Obtain Current Factor")
			def scalingFactorNumber = new ActiveLabel(factor, "-" )
			ActiveTextEnterField newScalingFactorNumber = new ActiveTextEnterField (null, injector, "")
			
			def textContainer = new Container()
			textContainer.setLayout(new GridLayout (3,2))
			textContainer.add(new Label("Current scaling factor: "))
			textContainer.add(scalingFactorNumber)
			textContainer.add(new Label("New scaling factor: "))
			textContainer.add(newScalingFactorNumber.getActiveTextField())
			textContainer.add(suspendButton)
			
			mainFrame.setLayout(new BorderLayout ())
			mainFrame.add(textContainer, BorderLayout.CENTER)
			
			mainFrame.pack()
			mainFrame.setVisible(true)
	// Update network to include the new active labels
			def network = [root, suspendButton, scalingFactorNumber, newScalingFactorNumber]
			new PAR(network).run()
		}
	
	}
	
