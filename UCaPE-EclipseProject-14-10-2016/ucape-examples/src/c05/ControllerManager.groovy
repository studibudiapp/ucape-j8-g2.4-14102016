package c05
      
// copyright 2012-13 Jon Kerridge
// Let's Do It In Parallel


import org.jcsp.lang.*
import java.awt.Label
import org.jcsp.awt.ActiveCanvas
import org.jcsp.awt.ActiveClosingFrame
import org.jcsp.awt.ActiveLabel
import org.jcsp.awt.ActiveTextField
import org.jcsp.groovy.*
 
class ControllerManager implements CSProcess 
{
	
  def ChannelInput factorIn 		// This receives the current factor from the scale process
  def ChannelOutput suspendOut		// This sends the suspend command to the scale process
  def ChannelOutput injectorOut		// This sends the new factor to be injected to the scale process
  def ChannelOutput factorOut		// This sends the current factor to the controllerManager process
  def ChannelInput suspendIn		// This receives the suspend command from the controllerManager process
  def ChannelInput injectorIn		// This receives the new factor from the controllerManager process
  
  
  void run() 
  {		
	def guards = new Alternative(suspendIn, factorIn, injectorIn)
	def preCon = new boolean[3]
	def SUSPEND = 0
	def FACTOR = 1
	def INJECTOR = 2
	preCon[SUSPEND] = true;
	preCon[FACTOR] = true;
	preCon[INJECTOR] = false;
	
    while (true) 
	{
		switch(guards.select())
		{
			case SUSPEND:
				def suspend = suspendIn.read()
				if(preCon[SUSPEND])
				{
					suspendOut.write ()                          // suspend signal to ScaleInt; value irrelevant
				}
				preCon[SUSPEND] = false;
				preCon[INJECTOR] = true;
			break;
			case FACTOR:
				factorOut.write(factorIn.read())
			break;
			case INJECTOR:
				try
				{
					def injector = Integer.parseInt(injectorIn.read())
					if(preCon[INJECTOR] )
					{
						injectorOut.write(injector)
						preCon[SUSPEND] = true;
						preCon[INJECTOR] = false;
					}
				}
				catch(Exception e)
				{
					
				}
			break;
		}
		
	}
  }
}
